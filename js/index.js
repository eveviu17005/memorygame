import { renderRandomOptions } from "./controller/controller.js";

const data = {
  id: 1,
  title: "Match the cards!",
  description: "You have 5 chances, use them well.",
  lives: 5,
  options: [
    {
      id: 1001,
      image: "https://images.gametize.com/Mono+Cross+Black+Topic+1.jpg",
      points: 10,
    },
    {
      id: 1002,
      image: "https://images.gametize.com/Mono+Stripe+Black+2.jpg",
      points: 10,
    },
    {
      id: 1003,
      image: "https://images.gametize.com/Mono+Lines+Black+3.jpg",
      points: 10,
    },
    {
      id: 1004,
      image: "https://images.gametize.com/Mono+Stripe+Black+4.jpg",
      points: 10,
    },
    {
      id: 1005,
      image: "https://images.gametize.com/Mono+Lines+Black+5.jpg",
      points: 10,
    },
  ],
};

let mainArr = [];
let pickArr = [];

// shuffle data.options
let shuffle = (array) => {
  let currentIndex = array.length,
    randomIndex;

  // While there remain elements to shuffle.
  while (currentIndex != 0) {
    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }

  return array;
};

// create mainArray for game
mainArr = shuffle([...shuffle(data.options), ...shuffle(data.options)]);

renderRandomOptions(mainArr);

let showAllCard = () => {
  let cards = document.querySelectorAll(".card");
  [...cards].forEach((item) => {
    item.classList.toggle("is-flipped");
  });
};

let hideAllCard = () => {
  let cards = document.querySelectorAll(".card");
  [...cards].forEach((item) => {
    item.classList.remove("is-flipped");
  });
};

// add class to flip card
let flipCardFunc = () => {
  let cards = document.querySelectorAll(".card");
  showAllCard();
  setTimeout(hideAllCard, 3000);

  [...cards].forEach((card) => {
    card.addEventListener("click", function () {
      card.classList.add("is-flipped");
      let pickCard = document.querySelectorAll(".is-flipped");
      console.log("pickCard: ", pickCard[0].dataset.id);
      console.log("pickCard: ", pickCard[1].dataset.id);
      if (pickCard.length % 2 == 0) {
      }
    });
  });
};
window.flipCardFunc = flipCardFunc;
