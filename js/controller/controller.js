export let renderRandomOptions = (dataOptions) => {
  let optionsCard = "";
  dataOptions.forEach((option, index) => {
    optionsCard += `<div class="scene scene--card">
        <div data-id="${option.id}" class="card">
          <div class="card__face card__face--front"></div>
          <div  class="card__face card__face--back"><img src="${option.image}" alt="" /></div>
        </div>
      </div>`;
  });
  document.getElementById("cardRender").innerHTML = optionsCard;
};
